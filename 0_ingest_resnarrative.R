# This file contains functions to load data for the residential_narrative_repot.

# The residential narrative relies on three main data sets:
# ---- Residential Assessments
# ---- Residential Ratios (contains sales)

# The functions below produce each of these data sets.
narrative_choices <- function(stage) {
  # The narrative user will make a selection that determines which set of values are output.
  choices <- tibble(
    choice = c(
      "Initial values before desk review",
      "Mailed initial values",
      "Assessor Certified values (after appeals)",
      "Board of Review certified values"
      ),
    column = c(
      "initial",
      "mailed",
      "assessor",
      "board"
    )
  )

  # Select which values you want to use for the report
  choice_column <- as.character(choices[choices$choice == stage, 2])
  
  return(choice_column)
  
}


# Convert between township number and township name
help_convert_town <- function(town_num, township_df) {
  if (nchar(town_num) != 2) {
    as.character(township_df$township_code[
      match(town_num, township_df$township_name)
      ])
  } else {
    as.character(township_df$township_name[
      match(town_num, township_df$township_code)
      ])
  }
}


# Convert between town number and triad
help_convert_tri <- function(town_num, township_df, name = FALSE) {
  if (!name) {
    if (nchar(town_num) != 2) {
      as.character(township_df$triad[match(town_num, township_df$township_name)])
    } else {
      as.character(township_df$triad[match(town_num, township_df$township_code)])
    }
  } else {
    if (nchar(town_num) != 2) {
      as.character(township_df$triad_name[match(town_num, township_df$township_name)])
    } else {
      as.character(township_df$triad_name[match(town_num, township_df$township_code)])
    }
  }
}


narrative_ingest_1 <- function(CCAODATA, geography, year, previous_stage, current_stage) {
  # This function queries the CCAO's SQL database and returns data on all residential assessments in the township
  # The parameter geography accepts a list of township names and passes these to the SQL server
  # The parameter year accepts a numeric or character vector containing YYYY years and passes these to the SQL server
  # The parameters for stages except a stage created by narrative_choices()
  # The function returns data that is unique by PIN & Year

  # Use case:
  # narrative_ingest_1(CCAODATA, 'Evanston', 2019, "assessor", "mailed")
  # returns a data frame with containing values for every PIN in Evanston township from 2015-2019

  data <- dbGetQuery(CCAODATA, paste0(
    "SELECT A.PIN AS pin
  , [PIPELINE RESULT] AS initial
  , [FIRST PASS] AS mailed
  , [CERTIFIED] AS assessor
  , [BOR RESULT] AS board
  , CASE WHEN CLASS IN (202, 203, 204, 205, 206, 207, 208, 209, 210, 234, 278, 295) THEN 'Single-Family'
    WHEN CLASS IN (211, 212) THEN 'Multi-Family'
    WHEN CLASS IN (299) THEN 'Residential Condominium'
    ELSE 'Something is wrong' END AS reporting_class
  , [YEAR] AS year
  , [NBHD] AS nbhd
  , [CLASS] AS class
  FROM VW_OPENDATA_ASSESSMENTS AS A
  INNER JOIN 
  FTBL_TOWNCODES AS B
  ON CONVERT(INT, A.TOWN)=B.township_code
  WHERE (1=1) 
  AND CLASS IN (202, 203, 204, 205, 206, 207, 208, 209, 210, 234, 278, 295, 211, 212, 299)
  AND township_name IN ('", geography, "') 
  AND A.YEAR >= (", as.numeric(year) - 5, ")"
  ))
  
  data <- data %>%
    select(
      pin, "prev_stage_fmv" = previous_stage, "curr_stage_fmv" = current_stage,
      "board_fmv" = board, reporting_class, year, nbhd, class
    ) %>%
    mutate(
      combined_fmv = ifelse(year == max(year), curr_stage_fmv, prev_stage_fmv)
    )

  return(data)
  
}


narrative_ingest_2 <- function(CCAODATA, geography, year, previous_stage, current_stage) {
  
  # returns sales ratios to be used in the sales ratio calculations
  data <- dbGetQuery(CCAODATA, paste0("
  SELECT [PIN] AS pin
  , [RATIO ON PIPELINE RESULT] AS initial
  , [RATIO ON FIRST PASS] AS mailed
  , [RATIO ON ASSESSOR CERTIFIED] AS assessor
  , [RATIO ON BOARD CERTIFIED] AS board
  , [PIPELINE VERSION] AS pipeline_version
  , [SALE PRICE] AS sale_price
  , [modeling_group] AS reporting_class
  , [NBHD] AS nbhd
  , [YEAR] AS year
  FROM VW_RES_RATIOS 
  WHERE (1=1)
  AND township_name IN ('", geography, "') 
  AND YEAR >= (", as.numeric(year) - 5, ")
  AND [SALE PRICE] >= 10000"))

  # If PIN has more than 1 Pipeline Version, take the latest one
  data <- data %>%
    group_by(pin, year) %>%
    filter(is.na(pipeline_version) | pipeline_version == max(pipeline_version)) %>%
    ungroup() %>%
    mutate(reporting_class = str_replace_all(reporting_class, "_", "-")) %>%
    select(
      pin, "prev_stage_ratio" = previous_stage, "curr_stage_ratio" = current_stage,
      "board_ratio" = board, sale_price, reporting_class, year, nbhd
    ) %>%
    mutate(
      prev_stage_fmv = prev_stage_ratio * sale_price,
      curr_stage_fmv = curr_stage_ratio * sale_price,
      board_fmv = board_ratio * sale_price
    ) %>%
    mutate(
      combined_ratio = ifelse(year == max(year), curr_stage_ratio, prev_stage_ratio),
      combined_fmv = ifelse(year == max(year), curr_stage_fmv, prev_stage_fmv)
    )
  
  return(data)
  
}


narrative_ingest_3 <- function(CCAODATA, geography, year) {
  
  data <- dbGetQuery(CCAODATA, paste0("
  SELECT SALES.PIN as pin
  , sale_date
  , sale_price
  , SALES.TAX_YEAR as year
  , CASE WHEN CLASS IN (202, 203, 204, 205, 206, 207, 208, 209, 210, 234, 278, 295) THEN 'Single-Family'
    WHEN CLASS IN (211, 212) THEN 'Multi-Family'
    WHEN CLASS IN (299) THEN 'Residential Condominium'
    ELSE 'Something is wrong' END AS reporting_class
  , town
  , nbhd
  FROM VW_CLEAN_IDORSALES AS SALES
  LEFT JOIN (
  	SELECT PIN, TAX_YEAR, LEFT(HD_TOWN, 2) AS town,
  	  HD_CLASS AS CLASS, HD_NBHD AS nbhd
  	FROM AS_HEADT
  ) AS HEADT ON SALES.PIN = HEADT.PIN AND SALES.TAX_YEAR = HEADT.TAX_YEAR
  INNER JOIN 
  FTBL_TOWNCODES AS B
  ON CONVERT(INT, HEADT.TOWN)=B.township_code
  WHERE SALES.TAX_YEAR >= (", as.numeric(year) - 5, ")
  AND township_name IN ('", geography, "') 
  AND sale_price >= 10000
  AND CLASS IN (202, 203, 204, 205, 206, 207, 208, 209, 210, 234, 278, 295, 211, 212, 299)"))
  
  return(data)
  
}


narrative_ingest_4 <- function(geography, year, reporting_class) {
  # Function to grab the township names and total count of properties for
  # each reporting class
  
  # Grab township df for the purposes of conversion
  township_df <- dbGetQuery(CCAODATA, "SELECT * FROM FTBL_TOWNCODES")
  
  # Convert reporting_class to the value used in the model outputs
  class_list <- list(
    "Single-Family" = "SF",
    "Multi-Family" = "MF",
    "Residential Condominium" = "NCHARS"
  )
  
  modeling_class <- class_list[names(class_list) == reporting_class]
  
  # Initial an empty list to store values
  results <- list()

  if (year <= 2019) {
    
    # Attach the townships used for modeling, if that information exists
    township_num <- help_convert_town(geography, township_df)
    township_file <- glue(
      "O:/CCAODATA/results/final_values/finalAV_report_{township_num}_{year}.xlsx"
    )
  
    # If the necessary modeling file exists, grab the townships it used
    if (file.exists(township_file)) {
      modeling_data <- read_xlsx(township_file, sheet = "Models")
  
      # Grab a list of townships used for modeling
      results[1] <- modeling_data %>%
        select(modeling_townships) %>%
        slice(1) %>%
        pull() %>%
        str_split(", ") %>%
        unlist() %>%
        map_chr(help_convert_town, township_df) %>%
        paste(collapse = ", ")
  
      # Grab the total number of observations used for modeling
      results[2] <- modeling_data %>%
        filter(modeling_group == modeling_class) %>%
        select(N) %>%
        pull() %>%
        comma()
  
      return(results)
      
    } else {
      print(glue("No modeling data was found for {geography} for {year}"))
      
      return(NA)
    }
    
  } else {
    triad_name <- help_convert_tri(geography, township_df, name = TRUE)
    
    # Find the most recent version of a triad file
    files_df <- file.info(list.files(
      "O:/CCAODATA/results/final_values/",
      pattern = glue("finalAV_report_{tolower(triad_name)}_tri_{year}"),
      full.names = TRUE))
    
    triad_file <- rownames(files_df)[which.max(files_df$mtime)]
    
    if (file.exists(triad_file) && length(triad_file) != 0) {
      modeling_data <- read_xlsx(triad_file, sheet = "Models")

      # Grab a list of townships used for modeling
      results[1] <- paste(triad_name, "Triad")
      
      # Grab the total number of observations used for modeling
      results[2] <- modeling_data %>%
        filter(modeling_group == modeling_class) %>%
        select(N) %>%
        pull() %>%
        comma()
      
      return(results)
      
    } else {
      print(glue("No modeling data was found for {geography} for {year}"))
      
      return(NA)
    }
    
  }
  
}

narrative_ingest_5 <- function(CCAODATA, geography, year, previous_stage, current_stage) {
  
  township_df <- dbGetQuery(CCAODATA, "SELECT * FROM FTBL_TOWNCODES")
  
  township_num <- help_convert_town(geography, township_df)
  
  data <- dbGetQuery(CCAODATA, paste0("
    SELECT
      prop_type,
      A.TAX_YEAR AS tax_year,
      SUM(A.initial) AS initial_sum,
      SUM(A.mailed) AS mailed_sum,
      SUM(A.assessor) AS assessor_sum,
      SUM(A.board) AS board_sum
    FROM (
        SELECT T.PIN, T.TAX_YEAR,
        M.fitted_value_6 / 10 AS initial,
    		T.HD_ASS_LND + T.HD_ASS_BLD AS mailed,
    		TB.HD_ASS_LND + TB.HD_ASS_BLD AS assessor,
    		BR.HD_ASS_LND + BR.HD_ASS_BLD AS board,
    		CASE WHEN LEFT(T.HD_CLASS,1) IN ('2') Then 'Residential'
    		  WHEN LEFT(T.HD_CLASS,1) IN ('1', '3', '4', '5', '6', '7', '9') 
    		  THEN 'Non-Residential' END AS prop_type
        FROM AS_HEADT T
        LEFT JOIN AS_HEADTB TB ON T.PIN = TB.PIN AND T.TAX_YEAR = TB.TAX_YEAR
        LEFT JOIN AS_HEADBR BR ON T.PIN = BR.PIN AND T.TAX_YEAR = BR.TAX_YEAR
        LEFT JOIN (
          SELECT a.PIN, a.TAX_YEAR, a.fitted_value_6
          FROM DTBL_MODELVALS a
          INNER JOIN (
              SELECT PIN, MAX(version) AS version
              FROM DTBL_MODELVALS
              GROUP BY PIN
          ) b ON a.PIN = b.PIN AND (a.version = b.version OR a.version IS NULL)
        ) M ON T.PIN = M.PIN AND T.TAX_YEAR = M.TAX_YEAR
        WHERE T.TAX_YEAR BETWEEN (", as.numeric(year) - 1, ") 
    	    AND (", as.numeric(year), ")
        AND LEFT(T.HD_TOWN, 2) IN ('", township_num, "')
    ) AS A
    WHERE A.prop_type IS NOT NULL
    GROUP BY A.TAX_YEAR, A.prop_type")
  )
  
  data <- data %>%
    arrange(tax_year) %>%
    group_by(tax_year) %>%
    mutate_at(vars(ends_with("sum")), list(pct = ~ .x / sum(.x))) %>%
    select(prop_type, tax_year, contains(previous_stage), contains(current_stage))
  
  
  return(data)
  
}
  
  
  
